console.log('Hello World!');

let trainer = Object();
trainer.name = "kyu";
trainer.age = 20;
trainer.catch = function() {
	console.log(`${name} caught a pokemon!`)
};
console.log(trainer);
console.log(trainer.name);
console.log(trainer.catch());
console.log(trainer['age']);

//constructor function 
function Pokemon(name){
	this.name = name;
	this.health = 100;
	this.attack = function(target){
		console.log(`${this.name} attacked ${target.name}`)
		target.health -=15;
		console.log(`${target.name}'s health is now ${target.health}`)
		console.log(`${target.name} goes for counter attack!`)
		this.health -=10;
		console.log(`${this.name}'s health is now ${this.health}.`)
	}
};

let squirtle = new Pokemon("squirtle");
let jigglypuff = new Pokemon("jigglypuff");

squirtle.attack(jigglypuff);